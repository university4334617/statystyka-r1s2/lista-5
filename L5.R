#Zadanie 1
zad1_a <- runif(5000, min = 0, max = 1)
hist(zad1_a, freq = FALSE, main = "Histogram - Rozkład Jednostajny", xlab = "Wartość", ylab = "Gęstość")
gestosc_j <- density(zad1_a)
plot(gestosc_j, main = "Estymator Jądrowy - Rozkład Jednostajny")

zad1_b <- rnorm(3000, mean = 100, sd = 15)
hist(zad1_b, freq = FALSE, main = "Histogram - Rozkład Normalny", xlab = "Wartość", ylab = "Gęstość")
gestosc_n <- density(zad1_b)
plot(gestosc_n, main = "Estymator Jądrowy - Rozkład Normalny")

#Zadanie 2
rzuty_kostka <- ceiling(runif(600) * 6)

sredni_wynik <- mean(rzuty_kostka)
sredni_wynik
wariancja <- var(rzuty_kostka)
wariancja

czestosci <- table(rzuty_kostka)
czestosci

ramka_danych <- as.data.frame(czestosci)
ramka_danych
wariancja_czestosci <- var(ramka_danych$Freq)
wariancja_czestosci

rzuty_kostka_sample <- sample(1:6, 600, replace = TRUE)
rzuty_kostka_sample

#Zadanie 3
k <- c(0, 1, 2, 3)
P_X_k <- c(0.3, 0.4, 0.2, 0.1)
sample_values <- sample(k, 1000, replace = TRUE, prob = P_X_k)
head(sample_values)

#Zadanie 4
binomial_realizations <- rbinom(100, size = 10, prob = 0.3)
print("Realizacje z rozkładu dwumianowego Bin(10, 0.3):")
print(binomial_realizations)

geometric_realizations <- rgeom(50, prob = 0.4)
print("Realizacje z rozkładu geometrycznego Geom(0.4):")
print(geometric_realizations)

#Zadanie 5
wyn<-array(0,dim=50)
srednia<-3
for (i in 1:50){
  u<-runif(1)
  p<-exp(-srednia)
  s<-p
  while (u>s){
    wyn[i]<-wyn[i]+1
    p<-p*srednia/wyn[i]
    s<-s+p
  }
}
wyn

#Zadanie 6
#a
F <- function(x) {
  if (x >= 0 && x <= 2) {
    return(0.25 * x^2)
  } else {
    return(0)
  }
}
wyn <- array(0, dim = 200)
for (i in 1:200) {
  u <- runif(1)
  x <- sqrt(4 * u)
  wyn[i] <- x
}
sr <- mean(wyn)
sr

#b
wyn<-array(0,dim=200)
for (i in 1:200){
  stop=0
  while (stop==0){
    x<-2*runif(1) 
    u<-runif(1)
    if (u<0.5*x) {stop<-1}
  }
  wyn[i]<-x
}
(sr<-mean(wyn))







